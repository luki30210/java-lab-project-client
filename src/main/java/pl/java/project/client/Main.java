package pl.java.project.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import pl.java.project.client.exception.NoConnectionException;
import pl.java.project.client.service.Rest;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {

        primaryStage.setTitle("Programowanie w języku Java - przeglądarka kodów źródłowych");
        primaryStage.setMinWidth(900);
        primaryStage.setMinHeight(600);
        primaryStage.getIcons().add(new Image("icon.png"));

        try {
            Rest.testConnection();
            try {
                this.showMainScene(primaryStage);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        } catch (NoConnectionException nce) {
            try {
                this.showNoConnectionScene(primaryStage);
            } catch (IOException ioe) {
                System.out.println("Błąd rysowania sceny noConnection");
            }
        }

    }

    public static void main(String[] args) {
        launch(args);
    }

    public void showMainScene(Stage stage) throws IOException {

        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("main_scene.fxml"));

        Scene mainScene = new Scene(root, 900, 600);

        stage.setScene(mainScene);
        stage.show();

    }

    public void showNoConnectionScene(Stage stage) throws IOException {

        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("no_connection_scene.fxml"));

        Scene noConnectionScene = new Scene(root, 900, 600);

        stage.setScene(noConnectionScene);
        stage.show();

    }

}
