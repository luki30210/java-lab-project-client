package pl.java.project.client.exception;

/**
 * Created by Łukasz Patro
 * on 24.06.17.
 */
public class NoConnectionException extends Exception {

    public NoConnectionException(String reason) {
        super(reason);
    }

    public NoConnectionException() {
        super("No connection with resource server.");
    }

}
