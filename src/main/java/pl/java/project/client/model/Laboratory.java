package pl.java.project.client.model;

import java.util.List;

/**
 * Created by Łukasz Patro
 * on 25.06.17.
 */
public class Laboratory {

    Long numer;

    String subject;

    List<Task> tasks;

    public Long getNumer() {
        return numer;
    }

    public void setNumer(Long numer) {
        this.numer = numer;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
