package pl.java.project.client.model;

/**
 * Created by Łukasz Patro
 * on 25.06.17.
 */
public class Task {

    public enum DifficultyLevel { easy, medium, hard }

    Long id;

    String name;

    DifficultyLevel difficultyLevel;

    Long LaboratoryNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DifficultyLevel getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(DifficultyLevel difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    public Long getLaboratoryNumber() {
        return LaboratoryNumber;
    }

    public void setLaboratoryNumber(Long laboratoryNumber) {
        LaboratoryNumber = laboratoryNumber;
    }

}
