package pl.java.project.client.controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import org.apache.commons.lang3.StringUtils;
import pl.java.project.client.model.Laboratory;
import pl.java.project.client.model.Section;
import pl.java.project.client.model.Task;
import pl.java.project.client.service.Rest;
import java.util.List;

/**
 * Created by Łukasz Patro
 * on 25.06.17.
 */
public class MainController {

    @FXML
    private TreeView<String> treeView;

    @FXML
    private TabPane tabPane;

    @FXML
    private Button copyButton;

    private Rest restService;

    private List<Laboratory> laboratories;

    private Long selectedTask = new Long(0);

    @FXML
    private void initialize() {

        this.restService = new Rest();

        this.initializeTreeView();

    }

    private void initializeTreeView() {

        this.laboratories = restService.getLaboratoryList();

        TreeItem rootItem = new TreeItem();
        rootItem.setExpanded(true);

        for (Laboratory laboratory : laboratories) {

            TreeItem<String> laboratoryItem = new TreeItem<String> (laboratory.getSubject());

            for (Task task : laboratory.getTasks()) {

                TreeItem<String> item = new TreeItem<String> (task.getName());
                laboratoryItem.getChildren().add(item);
            }

            rootItem.getChildren().add(laboratoryItem);

        }

        this.treeView.setRoot(rootItem);
        this.treeView.setShowRoot(false);

    }

    private void initializeTabPane(Task task) {

        tabPane.getTabs().clear();
        tabPane.setStyle("-fx-tab-max-height: 20px; -fx-font-size: 12px;");

        List<Section> taskSections = this.restService.getSections(task.getLaboratoryNumber(), task.getId());

        for (Section section : taskSections) {

            Tab tab = new Tab();
            tab.setClosable(false);
            String tabName = StringUtils.abbreviate(section.getName(), 20);
            tab.setText(tabName);

            BorderPane codeBorderPane = new BorderPane();
            TextArea textArea = new TextArea(section.getCode());
            textArea.setEditable(false);
            textArea.setStyle("-fx-border-color: white;");
            //textArea.setMinSize(this.tabPane.getWidth(), this.tabPane.getHeight());
            textArea.getStyleClass().add("code-text-area");

            Label descriptionLabel = new Label(section.getName());
            descriptionLabel.setStyle("-fx-font-size: 10px; -fx-font-style: italic;");

            codeBorderPane.setCenter(textArea);
            codeBorderPane.setTop(descriptionLabel);
            tab.setContent(codeBorderPane);

            tabPane.getTabs().add(tab);

            copyButton.setDisable(false);

        }

    }

    public void treeMouseClicked(MouseEvent mouseEvent) {

        if (!this.treeView.getSelectionModel().isEmpty()) {
            String selectedName = this.treeView.getSelectionModel().getSelectedItem().getValue();
            Task task = this.getTaskByName(selectedName);

            if (task != null && this.selectedTask != task.getId()) {
                this.initializeTabPane(task);
                this.selectedTask = task.getId();
            }
        }

    }

    private Task getTaskByName(String taskName) {
        for (Laboratory laboratory : this.laboratories) {
            for (Task task : laboratory.getTasks()) {

                if (task.getName().equals(taskName)) {
                    return task;
                }
            }
        }
        return null;
    }

    public void refreshLabList(MouseEvent mouseEvent) {
        this.initializeTreeView();
    }

    public void copySelectedCode(MouseEvent mouseEvent) {

        Integer selectedTabIndex = tabPane.getSelectionModel().getSelectedIndex();

        if (selectedTabIndex >= 0) {
            BorderPane borderPane = (BorderPane)tabPane.getSelectionModel().getSelectedItem().getContent();
            TextArea textArea = (TextArea)borderPane.getChildren().get(0);
            String code = textArea.getText();

            final Clipboard clipboard = Clipboard.getSystemClipboard();
            final ClipboardContent content = new ClipboardContent();
            content.putString(code);
            clipboard.setContent(content);
        }



    }
}