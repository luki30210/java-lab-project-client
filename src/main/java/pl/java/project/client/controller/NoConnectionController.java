package pl.java.project.client.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import pl.java.project.client.Main;
import pl.java.project.client.exception.NoConnectionException;
import pl.java.project.client.service.Rest;

import java.io.IOException;

/**
 * Created by Łukasz Patro
 * on 29.06.17.
 */
public class NoConnectionController {

    @FXML
    private Button button;

    public void refresh(MouseEvent mouseEvent) {

        try {
            Rest.testConnection();
            Main main = new Main();
            main.showMainScene((Stage)button.getScene().getWindow());
        } catch (NoConnectionException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

}
