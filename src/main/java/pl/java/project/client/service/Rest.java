package pl.java.project.client.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import javafx.beans.property.StringProperty;
import org.json.JSONArray;
import pl.java.project.client.exception.NoConnectionException;
import pl.java.project.client.model.Laboratory;
import pl.java.project.client.model.Section;
import pl.java.project.client.model.Task;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Łukasz Patro
 * on 25.06.17.
 */
public class Rest {

    private static final String restApiUrl = "http://localhost:8080";


    public static void testConnection() throws NoConnectionException {

        try {
            HttpResponse<JsonNode> jsonResponse = Unirest.get(Rest.restApiUrl).header("accept", "application/json").asJson();
        } catch (UnirestException e) {
            throw new NoConnectionException();
        }

    }

    public List<Laboratory> getLaboratoryList() {

        String url = this.restApiUrl + "/laboratory";
        List<Laboratory> laboratories = new ArrayList<Laboratory>();

        try {

            HttpResponse<JsonNode> jsonResponse = Unirest.get(url).header("accept", "application/json").asJson();
            JSONArray jsonArray = jsonResponse.getBody().getArray();

            for (int i = 0; i < jsonArray.length(); i++) {
                String subject = jsonArray.getJSONObject(i).get("subject").toString();
                Long numer = Long.parseLong(jsonArray.getJSONObject(i).get("number").toString());
                Laboratory laboratory = new Laboratory();
                laboratory.setNumer(numer);
                laboratory.setSubject(subject);
                laboratory.setTasks(this.getLaboratoryTasks(numer));
                laboratories.add(laboratory);
            }


        } catch (UnirestException e) {
            //return e.getMessage();
        }

        return laboratories;

    }

    public List<Task> getLaboratoryTasks(Long laboratoryNumber) {

        String url = this.restApiUrl + "/laboratory/" + laboratoryNumber + "/task";
        List<Task> tasks = new ArrayList<Task>();

        try {

            HttpResponse<JsonNode> jsonResponse = Unirest.get(url).header("accept", "application/json").asJson();

            if (jsonResponse.getStatus() != 200) {
                return tasks;
            }

            JSONArray jsonArray = jsonResponse.getBody().getArray();

            for (int i = 0; i < jsonArray.length(); i++) {
                Long id = Long.parseLong(jsonArray.getJSONObject(i).get("id").toString());
                String name = jsonArray.getJSONObject(i).get("name").toString();
                String difficultyLevel = jsonArray.getJSONObject(i).get("difficultyLevel").toString();
                Task task = new Task();
                task.setId(id);
                task.setName(name);
                task.setLaboratoryNumber(laboratoryNumber);
                if (difficultyLevel == "easy") task.setDifficultyLevel(Task.DifficultyLevel.easy);
                else if (difficultyLevel == "medium") task.setDifficultyLevel(Task.DifficultyLevel.medium);
                else task.setDifficultyLevel(Task.DifficultyLevel.hard);
                tasks.add(task);
            }


        } catch (UnirestException e) {
            //return e.getMessage();
        }

        return tasks;

    }

    public List<Section> getSections(Long laboratoryNumber, Long taskId) {

        String url = this.restApiUrl + "/laboratory/" + laboratoryNumber + "/task/" + taskId + "/section";
        List<Section> sections = new ArrayList<Section>();

        try {

            HttpResponse<JsonNode> jsonResponse = Unirest.get(url).header("accept", "application/json").asJson();

            if (jsonResponse.getStatus() != 200) {
                return sections;
            }

            JSONArray jsonArray = jsonResponse.getBody().getArray();

            for (int i = 0; i < jsonArray.length(); i++) {
                Long id = Long.parseLong(jsonArray.getJSONObject(i).get("id").toString());
                String name = jsonArray.getJSONObject(i).get("name").toString();
                String code = this.getCode(laboratoryNumber, taskId, id);
                Section section = new Section();
                section.setId(id);
                section.setName(name);
                section.setCode(code);
                sections.add(section);
            }


        } catch (UnirestException e) {
            //return e.getMessage();
        }

        return sections;

    }

    private String getCode(Long laboratoryNumber, Long taskId, Long sectionId) {

        String url = this.restApiUrl + "/laboratory/" + laboratoryNumber + "/task/" + taskId + "/section/" + sectionId + "/code";

        try {

            GetRequest getRequest = Unirest.get(url);

            if (getRequest.asString().getStatus() != 200) {
                return "";
            }

            //System.out.println(getRequest.asString().getStatus());
            return getRequest.asObject(String.class).getBody();


        } catch (UnirestException e) {

            System.out.println(e.getMessage());
        }

        return "";

    }

}
